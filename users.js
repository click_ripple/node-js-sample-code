const bcrypt = require('bcrypt');

const seq = require('../manageDB.js');
const saltRounds = 10;


module.exports = function(app){


	/* Get all registered users */
	app.get('/users', async(req, res) => {

		try{

			// if( !req.current_user.admin ){
			// 	return res.status(401).send( { status: false, message: "You are not allowed to perform this action" } );
			// }

			users = await seq.models.users.findAll();
			res.send( { status: true, data: users } );

		}
		catch(e){
			res.send( { status: false, message: e.message } );
		}

	})

	/* View User **/

	app.get('/users/profile', async(req, res) => {

		try{
			res.send( { status: true, data: req.current_user } ); 
		}
		catch(e){
			res.send( { status: false, message: e.message } );
		}

	})


	/***
	* Update my Profile
	*/
	app.put('/users/profile', async(req, res) => {

		try{
			
			let user = await seq.models.users.findOne( { where:{ id: req.current_user.id }});

			//check if user exists in db
			if( user === null ){
				return res.send( { status: false, message: "User not Found" } );
			}

			//update user data
			
			await seq.models.users.update( {firstname:req.body.firstname, lastname:req.body.lastname, phone:req.body.phone, address:req.body.address, address2:req.body.address2, city:req.body.city, state:req.body.state, zip:req.body.zip}, { where: {id: req.current_user.id } } );

			res.send( { status: true, message: "Proile updated successfully" } ); 

		}
		catch(e){
			res.send( { status: false, message: e.message } );
		}

	})

	/*** 
	* Update user profile
	* params: firstname, lastname, phone
	*/
	app.put('/users/profile/:id', async(req, res) => {

		try{

			if( req.current_user.id.toString() !== req.params.id.toString() && !req.current_user.admin ){
				return res.status(401).send( { status: false, message: "You are not allowed to perform this action" } );
			}
			
			let user = await seq.models.users.findOne( { where:{ id: req.params.id }});

			//check if user exists in db
			if( user === null ){
				return res.send( { status: false, message: "User not Found" } );
			}

			await seq.models.users.update( { firstname:req.body.firstname, lastname:req.body.lastname, phone:req.body.phone, address:req.body.address, address2:req.body.address1, city:req.body.city, state:req.body.state, zip:req.body.zip}, { where: {id: req.current_user.id } } );

			user = await seq.models.users.findOne( { where:{ id: req.params.id }});

			res.send( { status: true, message: "Proile updated successfully", data: user } ); 

		}
		catch(e){
			res.send( { status: false, message: e.message } );
		}

	});

//update password
	app.put('/users/password', async(req, res) => {

		try{

			let user = await seq.models.users.findOne( {where:{id: req.current_user.id }});

			//check old password for users not registered through google
			if( !req.current_user.is_google ){

				let valid = await bcrypt.compare( req.body.current, user.password );

				if( !valid ){
					return res.send( { status: false, message: "Current password is invalid." } );
				}
			}

			//compare new password and confirm password
			if( req.body.new !== req.body.confirm ){
				return res.send( { status: false, message: "Confirm password should match new password." } );
			}



			let hash = await bcrypt.hash(req.body.new, saltRounds);

			user.password = hash;
			user.is_google = false; //mark is_google as false if password generated

			await seq.models.users.update( {password:hash, is_google:false}, { where: {id: req.current_user.id } } );
			return res.send( { status: true, message: "Password successfully updated." } );

		}
		catch(e){
			res.send( { status: false, message: e.message } )
		}

	});



	app.post('/users/register', async(req, res) => {
		console.log(req.body);

	});



}