const seq = require('../manageDB.js');

const slugify = require('slugify');
const mailer = require('nodemailer');

var fs = require('fs');
const { promisify } = require('util')
const unlinkAsync = promisify(fs.unlink)

module.exports = function(app){

	/***
	* View File
	**/
	app.get('/files/:id', async(req, res) => {
		
		try{

			let file = await seq.models.files.findOne( {where: {id: req.params.id }} );
			//check if file exists in db
			if( file === null ){
				return res.send( { status: false, message: "File not Found" } );
			}
			
			//check if owner
			if( req.current_user.id.toString() !== file.userId.toString() ){
				return res.status(401).send( { status: false, message: "You are not allowed to perform this action" } );
			}

			res.send( { status: true, data: file } );

		}
		catch(e){
			res.send( { status: false, message: e.message } );
		}

	});

	/**
	* Download File
	***/
	app.get('/files/:id/download', async(req, res) => {

		let file = await seq.models.files.findOne( {where:{id: req.params.id }} );

		//check if file exists in db
		if( file === null ){
			return res.send( { status: false, message: "File not Found" } );
		}

		//check if owner
		if( req.current_user.id.toString() !== file.userId.toString() ){
			return res.status(401).send( { status: false, message: "You are not allowed to perform this action" } );
		}
		

		let contents = fs.readFileSync( file.path, 'base64');
		//let buffer = new Buffer(contents, 'base64');

		res.setHeader("Content-Type", file.type );
		//res.setHeader("Content-Length", buffer.length );
		res.send(contents);

	})


	// **
	// *	Rename the file
	// *   params: name
	// *
	app.put('/files/:id', async(req, res) => {
		
		let file = await seq.models.files.findOne({ where: {id: req.params.id }});
		
		//check if owner
		if( req.current_user.id.toString() !== file.userId.toString() ){
			return res.status(401).send( { status: false, message: "You are not allowed to perform this action" } );
		}


		if( req.body.name ){
			file.name = req.body.name;
            await seq.models.files.update( {name:req.body.name, slug: slugify(req.body.name, {lower: true})}, { where: {id: req.params.id } } );

		}
		

		res.send( { status: true, message: "File successfully updated", data: file } );

		//check if file exists in db
		if( file === null ){
			return res.send( { status: false, message: "File not Found" } );
		}

	});


	/***
	* Delete the file
	**/
	app.delete('/files/:id', async(req, res) => {
		try{
			

			let file = await seq.models.files.findOne({ where:{id: req.params.id }} );

			//check if file exists in db
			if( file === null ){
				return res.send( { status: false, message: "File not Found" } );
			}

			//check if owner
			if( req.current_user.id.toString() !== file.userId.toString() ){
				return res.status(401).send( { status: false, message: "You are not allowed to perform this action" } );
			}

			// Delete the file like normal
    		await unlinkAsync(file.path)
    		
			await seq.models.files.destroy({where:{id: req.params.id}});

			res.send( { status: true, message: "File successfully deleted" } );
		}
		catch(e){
			res.send( { status: false, message: e.message } );
		}
	})

	/***
	* Share File
	**/
	app.get('/files/:id/share', async(req, res) => {
		
		try{

			let file = await seq.models.files.findOne( {where: {id: req.params.id }} );

			let email = req.query.email;

			//check if file exists in db
			if( file === null ){
				return res.send( { status: false, message: "File not Found" } );
			}

			//check if owner
			if( req.current_user.id.toString() !== file.userId.toString() ){
				return res.status(401).send( { status: false, message: "You are not allowed to perform this action" } );
			}

			fs.readFile(file.path, function (err, data) {

				mailer.createTestAccount((err, account) => {
					//  smtp object
				    let transporter = mailer.createTransport({
				        host: 'smtp.gmail.com',
				        port: 587,
				        secureConnection: true,
				        auth: {
				            user: 'testdemotest11@gmail.com', 
				            pass: 'testeresfera.11'
				        }
				    });

				    let mailOptions = {
				        from: req.current_user.email,
				        to: email,
				        subject: 'Curischain app - Attachment!',
				        text: 'Please find the attached file.',
				        attachments: [{'filename': file.name, 'content': data}]
				    };

				    // send mail with defined transport object
				    transporter.sendMail(mailOptions, (error, info) => {
				    	
				        if (error) {
				            res.send( { status: false, message: 'Failed to share mail, Please try again later' } );
				        }

				        res.send( { status: true, message: 'File has shared successfully' } );

				    });
				});
			});
		}
		catch(e){
			res.send( { status: false, message: e.message } );
		}

	});

}