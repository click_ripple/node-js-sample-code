var multer  = require('multer');
const slugify = require('slugify');
var fs = require('fs');

var upload = multer({ dest: 'uploads/' })

var seq = require("../manageDB.js");


module.exports = function(app){

	/****** 
	** List all folders 
	*****/
	app.get('/folders', async(req, res) => {

		try{

			let folders = await seq.models.folders.findAll({ where: { userId: req.current_user.id }, include: [{ model: seq.models.files, attributes: ['size'] }], order: [[ 'createdAt', 'desc']] });

			res.send( { status: true, data: folders } );

		}
		catch(e){
			res.send( { status: false, message: e.message } );
		}

	});

	/*******
	** View Folder
	******/
	app.get('/folders/:id', async(req, res) => {

		try{

			let folder = await seq.models.folders.findOne( {where:{id: req.params.id }} );

			//check if folder exists in db
			if( folder === null ){
				return res.send( { status: false, message: "Folder not Found" } );
			}

			//check if owner
			if( req.current_user.id.toString() !== folder.userId.toString() ){
				return res.status(401).send( { status: false, message: "You are not allowed to perform this action" } );
			}

			res.send( { status: true, data: folder } );

		}
		catch(e){
			res.send( { status: false, message: e.message } );
		}

	});


	/***** 
	/* Create new folder 
	/* params:  name
	******/
	app.post('/folders', async(req, res) => {
		var folderId;
		try{
			
			//check for duplicate name
			let _folder = await seq.models.folders.findOne( { where: {name: req.body.name, userId: req.current_user.id } });

			if( _folder !== null ){
				return res.send( { status: false, message: "Folder already exists." } );
			}

			let folder =seq.models.folders.create({
				name: req.body.name,
				userId: req.current_user.id,
				slug: slugify(req.body.name, {lower: true})
			}).then(data=>{
				//console.log(data.id);
				_folder =  seq.models.folders.findOne( {where:{name: req.body.name, userId: req.current_user.id }} )
	            res.send( { status: true, message: "Folder successfully created", data: folder ,folderId: data.id } );
	           });
			
			

		}
		catch(e){

			res.send( { status: false, message: e.message } );
		}

	})

	/**** 
	/* Update folder
	/* params:  name
	***********/
	app.put('/folders/:id', async(req, res) => {
	
		try{
		
		 	let folder = await seq.models.folders.findOne( {where:{id: req.params.id , userId:req.current_user.id }});

		 	//check if folder exists in db
		 	if( folder === null ) {
		 		return res.send( { status: false, message: "Folder not Found" } );
		 	}

		 	else {
		 		folder.name = req.body.name;
		 		var value= await seq.models.folders.update( {name:req.body.name, slug: slugify(req.body.name, {lower: true})}, { where: {id: req.params.id } } );

		 		res.send( { status: true, message: "Folder successfully updated", data: folder } );

		 	}

		}
		catch(e){
		 	res.send( { status: false, message: e.message } );
		 }

	})

	/**** 
	/* Delete folder
	***********/
	app.delete('/folders/:id', async(req, res) => {
		try{
			
			let folder = await seq.models.folders.findOne( {where :{id: req.params.id}} );
		 	//check if folder exists in db
		 	if( folder === null ) {
		 		return res.send( { status: false, message: "Folder not Found" } );
		 	}

		 	//check if owner
			if( req.current_user.id.toString() !== folder.userId.toString() ){
				return res.status(401).send( { status: false, message: "You are not allowed to perform this action" } );
			}
			
			else {

				let files= await seq.models.files.findAll( {where :{folderId: req.params.id}}).then(function(data){
					for(i=0;i<data.length;i++) {
						console.log(data[i].dataValues.path)
					   fs.unlinkSync(data[i].dataValues.path);
					}
				})
				await seq.models.files.destroy({where:{folderId: req.params.id}});
				await seq.models.folders.destroy({where:{id: req.params.id}});
				res.send( { status: true, message: "Folder successfully deleted" } );
		 	}
		}
		catch(e){
			console.log('error',e)
			res.send( { status: false, message: e.message } );
		}

	});



	/******
	* Upload file to folder
	*****/
	app.post('/folders/:id/files', upload.single('file'),  async(req, res) => {

		try{

			let folder = await seq.models.folders.findOne( {where:{id: req.params.id } });

			//check if folder exists in db
			if( folder === null ){
				return res.send( { status: false, message: "Folder not Found" } );
			}

			//check if owner
			if( req.current_user.id.toString() !== folder.userId.toString() ){
				return res.status(401).send( { status: false, message: "You are not allowed to perform this action" } );
			}

			if( req.file !== undefined ){

				let file = seq.models.files.create({
					folderId: req.params.id,
					userId: req.current_user.id,
					name:req.file.originalname,
					type:req.file.mimetype,
					path:req.file.path,
					size:req.file.size,
					slug: slugify(req.file.originalname, {lower: true})

				})

				return res.send( { status: true, message: "File successfully uploaded", data: file } );

			}
			else{
				return res.send( { status: false, message: "No file was uploaded" } );
			}

			

		}
		catch(e){
			res.send( { status: false, message: e.message } );
		}

	});




	/***
	* List all files in folder
	**/
	app.get('/folders/:slug/files', async(req, res) => {

		try{

			let folder = await seq.models.folders.findOne( {where: { slug: req.params.slug, userId: req.current_user.id }} );

			//check if folder exists in db
			if( folder === null ) {
				return res.send( { status: false, message: "Folder not Found" } );
			}
			
			//check if owner
			// if( req.current_user.id.toString() !== folder.userId.toString() ){
			// 	return res.status(401).send( { status: false, message: "You are not allowed to perform this action" } );
			// }

			let files = await seq.models.files.findAll({ where: { folderId: folder.id, userId: req.current_user.id } });

			return res.send( { status: true, data: { files: files, folder: folder } } );

		}
		catch(e){
			res.send( { status: false, message: e.message } );
		}

	});

}